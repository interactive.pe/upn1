
import axios from 'axios';

import { API } from './valores'

import { store } from 'store/'

export const buscar = (valor) => {

	const url = `${API}/prospecto/buscar/${valor}`
	return fetch(url)
		.then(response => response.json())
}

export const registrar = (info) => {

	const url = `${API}/prospecto`
	const headers = { 'Content-Type': 'application/json' }
	return axios.post(url, info, { headers })
	.then(response => response.data)
}
