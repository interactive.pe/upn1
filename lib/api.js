export const facultades = [
	{
		nombre:"derecho",
		titulo: "DERECHO",
		imagen: "derecho.jpg",
		imagenMovil: "derecho-movil.jpg",
		puntos:[
			{
				x: 850,
				y: 511,
				url: "videojuego",
				texto: "Video juego",
				direccion: "abajo"
			},{
				x: 700,
				y: 166,
				url: "charlas",
				texto: "Charlas",
				direccion: "arriba"
			},{
				x: 420,
				y: 362,
				url: "carreras",
				texto: "Carreras",
				direccion: "izquierda"
			},{
				x: 570,
				y: 511,
				url: "testimoniales",
				texto: "Testimoniales",
				direccion: "abajo"
			}
		],
		puntosMovil:[
			{
				x: 207,
				y: 170,
				url: "videojuego",
				texto: "Video juego",
				direccion: "abajo"
			},{
				x: 120,
				y: 400,
				url: "charlas",
				texto: "Charlas",
				direccion: "arriba"
			},{
				x: 340,
				y: 306,
				url: "carreras",
				texto: "Carreras",
				direccion: "izquierda"
			},{
				x: 267,
				y: 430,
				url: "testimoniales",
				texto: "Testimoniales",
				direccion: "abajo"
			}
		],
		testimoniales: [{
			id: "LwvUwjsVkig",
			titulo: "Alumno"
		},{
			id: "45CNZ7X-nB4",
			titulo: "Ex alumno"
		}],
		charla:"0G1Y9AD8uBg",
		carreras: [
			"Derecho"
		]
	},{
		nombre:"comunicaciones",
		titulo: "COMUNICACIONES",
		imagen: "comunicaciones.jpg",
		puntos:[
			{
				x: 990,
				y: 410,
				url: "videojuego",
				texto: "Video juego",
				direccion: "abajo"
			},{
				x: 850,
				y: 561,
				url: "charlas",
				texto: "Charlas",
				direccion: "arriba"
			},{
				x: 570,
				y: 561,
				url: "carreras",
				texto: "Carreras",
				direccion: "izquierda"
			},{
				x: 250,
				y: 400,
				url: "testimoniales",
				texto: "Testimoniales",
				direccion: "abajo"
			}
		],
		imagenMovil: "comunicaciones-movil.jpg",
		puntosMovil:[
			{

				x: 330,
				y: 290,
				url: "videojuego",
				texto: "Video juego",
				direccion: "abajo"
			},{
				x: 90,
				y: 420,
				url: "charlas",
				texto: "Charlas",
				direccion: "arriba"
			},{
				x: 90,
				y: 200,
				url: "carreras",
				texto: "Carreras",
				direccion: "izquierda"
			},{
				x: 310,
				y: 420,
				url: "testimoniales",
				texto: "Testimoniales",
				direccion: "abajo"
			}
		],
		testimoniales: [{
			id: "X86JHoPBgLE",
			titulo: "Alumno"
		},{
			id: "MoVbimYLeNE",
			titulo: "Ex alumno"
		}],
		charla:"nsTwIAR473M",
		carreras: [
			"Com y Publicidad",
			"Com y Periodismo",
			"Com Audiovisual en Medios Digitales",
			"Com Corporativa",
			"Comunicación",
			"Educación y Gestión del Aprendizaje",
			"Comunicación y Diseño Gráfico",
			"Comunicación y Mktg Digital ",
		]

	},{
		nombre:"arquitecturadisegno",
		titulo: "ARQUITECTURA Y DISEÑO",
		imagen: "arquitecturadisegno.jpg",
		puntos:[
			{
				x: 720,
				y: 280,
				url: "videojuego",
				texto: "Video juego",
				direccion: "abajo"
			},{
				x: 950,
				y: 521,
				url: "charlas",
				texto: "Charlas",
				direccion: "arriba"
			},{
				x: 350,
				y: 551,
				url: "carreras",
				texto: "Carreras",
				direccion: "izquierda"
			},{
				x: 1280,
				y: 580,
				url: "testimoniales",
				texto: "Testimoniales",
				direccion: "abajo"
			}
		],
		imagenMovil: "arquitecturadisegno-movil.jpg",
		puntosMovil:[
			{
				x: 150,
				y: 190,
				url: "videojuego",
				texto: "Video juego",
				direccion: "abajo"
			},{
				x: 90,
				y: 390,
				url: "charlas",
				texto: "Charlas",
				direccion: "arriba"
			},{
				x: 320,
				y: 170,

				url: "carreras",
				texto: "Carreras",
				direccion: "izquierda"
			},{
				x: 330,
				y: 370,
				url: "testimoniales",
				texto: "Testimoniales",
				direccion: "abajo"
			}
		],
		testimoniales: [{
			id: "6Rmkeizyvvo",
			titulo: "Alumno"
		},{
			id: "l8r9EGo2BLQ",
			titulo: "Ex alumno"
		}],
		charla:"y1oWoZnLMMo",
		carreras: [
			"Arquitectura y Diseño de Interiores",
			"Arquitectura y Urbanismo",
			"Diseño Industrial",
		]
	},{
		nombre: "ingenieria",

		titulo: "INGENIERÍA",
		imagen: "ingenieria.jpg",
		puntos:[
			{
				x: 850,
				y: 250,
				url: "videojuego",
				texto: "Video juego",
				direccion: "abajo"
			},{
				x: 520,
				y: 420,
				url: "charlas",
				texto: "Charlas",
				direccion: "arriba"
			},{
				x: 290,
				y: 315,
				url: "carreras",
				texto: "Carreras",
				direccion: "izquierda"
			},{
				x: 560,
				y: 200,
				url: "testimoniales",
				texto: "Testimoniales",
				direccion: "abajo"
			}
		],
		imagenMovil: "ingenieria-movil.jpg",
		puntosMovil:[
			{
				x: 210,
				y: 270,
				url: "videojuego",
				texto: "Video juego",
				direccion: "abajo"
			},{

				x: 330,
				y: 410,
				url: "charlas",
				texto: "Charlas",
				direccion: "arriba"
			},{
				x: 90,
				y: 180,

				url: "carreras",
				texto: "Carreras",
				direccion: "izquierda"
			},{
				x: 80,
				y: 390,
				url: "testimoniales",
				texto: "Testimoniales",
				direccion: "abajo"
			}
		],
		testimoniales: [{
			id: "JNWtj4AbURk",
			titulo: "Alumno"
		},{
			id: "OCOz6_mW3mQ",
			titulo: "Ex alumno"
		}],
		charla:"cBL7GfmY_6Q",
		carreras: [
			"Ingeniería Industrial",
			"Ingeniería De Sistemas Comp",
			"Ingeniería Empresarial",
			"Ingeniería Agroindustrial",
			"Ingeniería Civil",
			"Ingeniería Ambiental",
			"Ingeniería Electrónica",
			"Ingeniería Mecatrónica",
			"Ingeniería Minas",
			"Ingeniería Logística y Transporte",
			"Ingeniería Geológica",
		]
	},{
		nombre: "negocios",
		titulo: "NEGOCIOS",
		imagen: "negocios.jpg",
		puntos:[
			{
				x: 580,
				y: 450,
				url: "videojuego",
				texto: "Video juego",
				direccion: "abajo"
			},{
				x: 920,
				y: 351,
				url: "charlas",
				texto: "Charlas",
				direccion: "arriba"
			},{
				x: 1080,
				y: 281,
				url: "carreras",
				texto: "Carreras",
				direccion: "izquierda"
			},{
				x: 290,
				y: 315,
				url: "testimoniales",
				texto: "Testimoniales",
				direccion: "abajo"
			}
		],
		imagenMovil: "negocios-movil.jpg",
		puntosMovil:[
			{
				x: 320,
				y: 420,
				url: "videojuego",
				texto: "Video juego",
				direccion: "abajo"
			},{
				x: 100,
				y: 330,
				url: "charlas",
				texto: "Charlas",
				direccion: "arriba"
			},{
				x: 330,
				y: 210,
				url: "carreras",
				texto: "Carreras",
				direccion: "izquierda"
			},{
				x: 100,
				y: 170,
				url: "testimoniales",
				texto: "Testimoniales",
				direccion: "abajo"
			}
		],
		testimoniales: [{
			id: "Te8FE_B7P9k",
			titulo: "Alumno"
		},{
			id: "P3Un5lRDHs4",
			titulo: "Ex alumno"
		}],
		charla:"u3hysYJxgUU",
		carreras: [
			"Administración y Marketing",
			"Administración y Servicios Turísticos",
			"Administración y Gestión Comercial",
			"Administración Bancaria y Financiera",
			"Contabilidad y Finanzas",
			"Economía y Negocios Internacionales",
			"Economía",
			"Administración",
			"Administración y NII",
			"Gastronomía y Gestión de Restaurantes",
			"Adm y Gestión del Talento Humano",
			"Adm y Gestión Pública",
			"Adm y Gestión del Emprendimiento",
		]
	},{
		nombre:"salud",
		titulo: "SALUD",
		imagen: "salud.jpg",
		puntos:[
			{
				x: 590,
				y: 360,
				url: "videojuego",
				texto: "Video juego",
				direccion: "abajo"
			},{
				x: 300,
				y: 500,
				url: "charlas",
				texto: "Charlas",
				direccion: "arriba"
			},{
				x: 1070,
				y: 550,
				url: "carreras",
				texto: "Carreras",
				direccion: "izquierda"
			},{
				x: 660,
				y: 560,
				url: "testimoniales",
				texto: "Testimoniales",
				direccion: "abajo"
			}
		],
		imagenMovil: "salud-movil.jpg",
		puntosMovil:[
			{
				x: 320,
				y: 420,
				url: "videojuego",
				texto: "Video juego",
				direccion: "abajo"
			},{
				x: 90,
				y: 380,
				url: "charlas",
				texto: "Charlas",
				direccion: "arriba"
			},{
				x: 100,
				y: 230,
				url: "carreras",
				texto: "Carreras",
				direccion: "izquierda"
			},{
				x: 320,
				y: 230,
				url: "testimoniales",
				texto: "Testimoniales",
				direccion: "abajo"
			}
		],
		testimoniales: [{
			id: "l2d0KL22fxw",
			titulo: "Alumno"
		},{
			id: "Juf4Algl5Tc",
			titulo: "Ex alumno"
		}],
		charla:"2PWKd_HrbYs",
		carreras: [
			"Enfermería",
			"Psicología",
			"Obstetricia",
			"Nutrición y Dietética",
			"Terapia Física y Rehabilitación",

		]
	}
]

export const obtenerFacultad = nombre => facultades.find( facultad => facultad.nombre == nombre)


export const telefonos = [
	"989041252",
	"969384450",
	"992372307",
	"989481185",
	// "987126502",
	"955571523",
	"976386608",
	"969383009",
	"948332370",
	"978371446"
]
