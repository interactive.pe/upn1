import ReactGA from 'react-ga'
import axios from 'axios';
import { API } from 'api/valores'
export const iniciarGA = () => {
	// console.log('GA init')
	// ReactGA.initialize('G-X2GQ4GYR8Y')
	ReactGA.initialize(process.env.NEXT_PUBLIC_GA_ID)
}
export const registrarPaginaActual = () => {
	// console.log(`Logging pageview for ${window.location.pathname}`)
	ReactGA.set({ page: window.location.pathname + window.location.search })
	ReactGA.pageview(window.location.pathname + window.location.search)

	axios.post(`${API}/analitica/registrar`, {
		categoria: "pagina",
		nombre: "pagina",
		url: window.location.pathname + window.location.search
	},
	{ 'Content-Type': 'application/json' })
}
export const registrarPagina = (url) => {
	// console.log(`Logging pageview for ${window.location.pathname}`)
	ReactGA.set({ page: url })
	ReactGA.pageview(url)

	axios.post(`${API}/analitica/registrar`, {
		categoria: "pagina",
		nombre: "pagina",
		url: url
	},
	{ 'Content-Type': 'application/json' })
}
export const registrarEvento = (category = '', action = '') => {
	if (category && action) {
		ReactGA.event({ category, action })

		axios.post(`${API}/analitica/registrar`, {
			categoria: category,
			nombre: action,
			url: window.location.pathname + window.location.search
		},
		{ 'Content-Type': 'application/json' })


	}
}
export const registrarExcepcion = (description = '', fatal = false) => {
	if (description) {
		ReactGA.exception({ description, fatal })
	}
}
