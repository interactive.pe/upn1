import styles from './menu.module.css'
import Link from 'next/link'
//<div className={styles.facultades}>FACULTADES</div>
export default function Menu({ children }) {
  return <div className={styles.container}>
  	<div className={styles.lcontainer}>
  		<div className={styles.lbl_white}>BIENVENIDO</div>
  		<div className={styles.lbl_yellow} style={{marginBottom:10}}>CARLOS</div>
  		<div className={styles.lbl_white}>TIENES</div>
  		<div className={styles.lbl_yellow} style={{marginBottom:10}}>90 PUNTOS</div>
      <Link href="/">
      <div className={styles.lbl_white}>cerrar sesión2</div>
      </Link>
  		<div className={styles.facultades_container}>
  			<div className={styles.facultades}>FACULTADES</div>
  		</div>


  		<Link href="https://www.facebook.com/UPN">
  			<img src="/facebook.svg" className={styles.fb_ico}/>
  		</Link>
  		<Link href="https://www.instagram.com/upn/?hl=es-la">
  			<img src="/instagram.svg" className={styles.ig_ico}/>
  		</Link>
  		<div className={styles.lbl_politicas}>políticas y privacidad</div>
  	</div>
  	<div>
  		<ul>
	    	<li>NEGOCIOS</li>
	    	<li>INGENIERÍA</li>
	    	<li>COMUNICACIONES</li>
	    	<li>SALUD</li>
	    	<li>ARQUITECTURA Y DISEÑO</li>
	    	<li>DERECHO</li>
	    </ul>
  	</div>


  </div>
}
