import { useState } from 'react';
import styles from './estilo.module.css'
import classnames from 'classnames'

import { useForm } from 'react-hook-form'
import isEmail from 'validator/lib/isEmail'
import { buscar } from 'api/prospecto'

import Spinner from 'react-bootstrap/Spinner'

// function validarCorreo(correo) {
// 	const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
// 	return re.test(String(correo).toLowerCase());
// }

// const urlBase = "//45.32.166.88/upn/servicios";
// const urlBase = "//api.upnconecta.pe";
const urlBase = process.env.NEXT_PUBLIC_URL_API

export default function Loginbox({ onContinuar }) {

	const { register, handleSubmit, errors } = useForm()
	const [telefono, cambiarTelefono] = useState("")
	const [correo, cambiarCorreo] = useState("")
	const [error, actualizarError] = useState("")
	const [cargando, actualizarCargando] = useState(false)

	const continuar = data => {
		console.log(data)
		actualizarCargando(true)

		if (data.telefono||data.correo) {
			buscar(data.telefono||data.correo)
				.then(rpta => {
					// if( rpta.estado && rpta.estado == "ERROR" ){
					// 	//actualizarError("Datos no encontrados")


					// }
					// else{
					// 	onContinuar(rpta)
					// }
					onContinuar(rpta)
					actualizarCargando(false)
				})
		}
		else{
			actualizarError("Ingresa tu celular o tu correo")
		}
		return;



		// if (data.telefono) {
		// 	fetch(`${urlBase}/prospecto/buscar/${data.telefono}`)
		// 		.then((response) => {
		// 			return response.json()
		// 		})
		// 		.then((json) => {
		// 			console.log(json.datos)
		// 			onContinuar(json)
		// 		})

		// }else if(data.correo){

		// 	fetch(`${urlBase}/prospecto/buscar/${data.correo}`)
		// 		.then((response) => {
		// 			return response.json()
		// 		})
		// 		.then((json) => {
		// 			console.log(json.datos)
		// 			onContinuar(json)
		// 		})
		// }else{
		// 	alert("Ingresa el celular de tu apoderado o tu correo")
		// }

		// onContinuar()
	}


	return <>
		<div className={styles.container}>
			<div className={styles.barra}></div>
			<h1>INGRESA TUS DATOS</h1>
			<p>para entrar a UPN CONECTA debes <br/>ingresar tu celular o tu correo.</p>
			<form onSubmit={handleSubmit(continuar)}>
				<div className={styles.frm_control}>
					Tu celular
					<input
						name="telefono"
						type="tel"
						ref={register}
						maxLength="9"
						minLength="9"
					/>
					{errors.telefono && <p>{errors.telefono.message}</p>}
				</div>
				<div className={styles.frm_control}>
					Tu correo
					<input
						name="correo"
						type="email"
						ref={register}
					/>
				</div>
				<If condition={error.length}>
					<div className={styles.error}>{error}</div>
				</If>
				<button type="submit" className={styles.boton}>
					<span>INGRESA</span>
					<Spinner className={classnames(styles.spinner, {[styles.activo]: cargando})} animation="border" size="sm" />
				</button>
			</form>
		</div>
	</>
}
