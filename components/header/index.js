import css from './estilo.module.sass'
import classnames from 'classnames';
import Link from 'next/link'

import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Image from 'react-bootstrap/Image'
import Button from 'react-bootstrap/Button'

import Menu from 'components/menu'


// export default function component({className}) {
// 	return <Container fluid className={classnames(className)}>
// 		<Row className={classnames("justify-content-between")}>
// 			<Col className={classnames("py-2")}>
// 				<Image className={css.logo} src="img/logotema.png" fluid/>
// 			</Col>
// 			<Col xs="auto" className={classnames("py-2")}>
// 				<Button className={classnames("border-0", "rounded-circle", "p-0")}>
// 					<Image src="img/menu.svg" />
// 				</Button>
// 			</Col>
// 		</Row>
// 	</Container>
// }

const component = ({className, menuClick}) => (
	<div className={classnames(css.contenedor, className)}>
		<div className={css.marca}>
			<Link href="/inicio">
				<img src="/img/logotema.png" alt=""/>
			</Link>
		</div>
	</div>
)


export default component
