import React, { Component } from 'react';
import styles from './nivel.module.css';
import Button from '../button';
export default class Nivel extends Component {
  constructor(props) {
        super(props);
  }
  /*
  <div className={styles.containerNivel}>
    <div className={styles.iconCandadoOpen}></div>
    <img src="/img/razones.svg"/>
    <Button style={{marginTop:12,width:120}} texto="NIVEL 1"></Button>
  </div>
  <div className={styles.containerNivel}>
    <div className={styles.iconCandadoOpen}></div>
    <img src="/img/razones.svg"/>
    <Button style={{marginTop:12,width:120}} texto="NIVEL 1"></Button>
  </div>
  <div className={styles.containerNivel}>
    <div className={styles.iconCandadoOpen}></div>
    <img src="/img/razones.svg"/>
    <Button style={{marginTop:12,width:120}} texto="NIVEL 1"></Button>
  </div>
  */
  render() {
    return (<div className={styles.container}>
              <div className={styles.closeButton}></div>

              <div className={styles.containerNivel}>
                <div className={styles.iconCandadoOpen}></div>
                <img src="/img/razones.svg"/>
                <Button style={{marginTop:12,width:120}} texto="NIVEL 1"></Button>
              </div>
              <div className={styles.containerNivel}>
                <div className={styles.iconCandadoClose}></div>
                <img src="/img/razones.svg"/>
                <Button style={{marginTop:12,width:120}} texto="NIVEL 2"></Button>
              </div>
              <div className={styles.containerNivel}>
                <div className={styles.iconCandadoClose}></div>
                <img src="/img/razones.svg"/>
                <Button style={{marginTop:12,width:120}} texto="NIVEL 3"></Button>
              </div>
              <div className={styles.containerNivel}>
                <div className={styles.iconCandadoClose}></div>
                <img src="/img/razones.svg"/>
                <Button style={{marginTop:12,width:120}} texto="NIVEL 4"></Button>
              </div>


            </div>);
  }
}
