import React, { Component } from 'react';
import { useState, useEffect } from 'react'
import css from './estilo.module.sass'
import classnames from 'classnames';
import axios from 'axios';

import { useSelector, shallowEqual, useDispatch } from 'react-redux'
import { actualizarPuntaje } from 'store/puntaje/accion'

// const urlBase = "//api.upnconecta.pe";
const urlBase = process.env.NEXT_PUBLIC_URL_API;

class CitaPop extends Component {
	constructor(props) {
	    super(props);
	    this.state = {nombre_colegio: '',fecha_hora:'',plataforma:'Zoom',error:''};
			this.onPressEnviar = this.onPressEnviar.bind(this);
	}
	onPressEnviar(){
			var params = {};
			params.usuario = 1;
			params.nombre_colegio = this.state.nombre_colegio;
			params.fecha_hora = this.state.fecha_hora;
			params.plataforma = this.state.plataforma;
			this.setState({error:""});
			if(params.nombre_colegio==""){
				this.setState({error:"El nombre del colegio es requerido."});
				return
			}
			if(params.fecha_hora==""){
				this.setState({error:"El fecha es requerida."});
				return
			}
			this.setState({error:"Enviando ..."});

			const headers = { 'Content-Type': 'application/json' }
			axios.post(`${urlBase}/cita/crear`, params, { headers })
				.then(response => {
					if(response.data.estado=="OK"){
						this.setState({error:"Cita vocacional solicitada con exito"});
						this.props.hito()
					}
				})
	}
  render() {
    return (<div className={css.citaPop}>
			<h5>CITA VOCACIONAL</h5>
			<div className={css.label}>Nombre de colegio*</div>
			<input className={css.frmInput} type="text" value={this.state.value} onChange={(e)=>{ this.setState({nombre_colegio: e.target.value}); }} />
			<div className={css.label}>Elige la fecha y hora*</div>
			<select onChange={(e)=>{ this.setState({fecha_hora: e.target.value}); }}>
			<option value="">Selecciona</option>
			<optgroup label="Trujillo:">
				<option value="Trujillo:Lunes 09 Nov. 9:00 am">Lunes 09 Nov. 9:00 am</option>
				<option value="Trujillo:Lunes 16 Nov. 9:00 am">Lunes 16 Nov. 9:00 am</option>
				<option value="Trujillo:Miércoles 11 Nov. 4:00 pm">Miércoles 11 Nov. 4:00 pm</option>
				<option value="Trujillo:Miércoles 18 Nov. 4:00 pm">Miércoles 18 Nov. 4:00 pm</option>
			</optgroup>
			<optgroup label="Cajamarca:">
				<option value="Cajamarca:Lunes 09 Nov. 9:00 am">Lunes 09 Nov. 9:00 am</option>
				<option value="Cajamarca:Lunes 09 Nov. 4:00 pm">Lunes 09 Nov. 4:00 pm</option>
				<option value="Cajamarca:Miércoles 11 Nov. 9:00 am">Miércoles 11 Nov. 9:00 am</option>
				<option value="Cajamarca:Miércoles 11 Nov. 4:00 pm">Miércoles 11 Nov. 4:00 pm</option>
				<option value="Cajamarca:Viernes 13 Nov. 9:00 am">Viernes 13 Nov. 9:00 am</option>
				<option value="Cajamarca:Viernes 13 Nov. 4:00 pm">Viernes 13 Nov. 4:00 pm </option>
			</optgroup>

			<optgroup label="Lima:">
				<option value="Lima:Lunes 16 Nov. 10:00 am">Lunes 16 Nov. 10:00 am</option>
				<option value="Lima:Lunes 23 Nov. 4:00 pm">Lunes 23 Nov. 4:00 pm</option>
			</optgroup>
			</select>
			<div className={css.label}>Elige la plataforma de tu preferencia*</div>
			<select onChange={(e)=>{ this.setState({plataforma: e.target.value}); }}>
				<option value="Zoom">Zoom</option>
				<option value="Google Meet">Google Meet</option>
				<option value="Otras">Otras</option>
			</select>
			<div className={css.error}>{this.state.error}</div>
			<button className={css.button} value="Enviar" onClick={this.onPressEnviar}>Enviar</button>
		</div>);
	}
}

const component = ({className, cerrar}) => {

	const [hito, actualizarHito] = useState(false)
	const dispatch = useDispatch()

	const asignarPuntaje = () => {
		dispatch(actualizarPuntaje("cita", 100))
	}

	return <>
		<div className={css.cuadro}>
			<div className={css.contenedor}>
				<button className={css.cerrar} onClick={cerrar}>
					<img src="/img/close.svg" alt=""/>
				</button>
				<div className={css.contentTexto}>
					<div className={css.texto}>
				Te ayudaremos a  reconocer tus capacidades, aptitudes y actitudes, con las que lograrás identificar tu vocación.
				<br/><br/>
				Completa tus datos para que nuestro consejero se comunique contigo
				</div>
				</div>
				<div className={css.separador}></div>
				<div className={css.contenido}>
					<CitaPop hito={asignarPuntaje}></CitaPop>
				</div>
			</div>
		</div>
	</>
}

export default component
