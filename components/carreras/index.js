import { useState, useEffect } from 'react'
import css from './estilo.module.sass'
import classnames from 'classnames';

const component = ({className, cerrar, facultad, titulo_facultad}) => {
	return <>
		<div className={css.cuadro}>
			<div className={css.contenedor}>

				<div className={css.contenido}>
					<button className={css.cerrar} onClick={cerrar}>
						<svg width="60" height="60" viewBox="0 0 60 60" fill="none" xmlns="http://www.w3.org/2000/svg">
						<circle cx="30" cy="30" r="30" fill="black"/>
						<rect x="18" y="39.8996" width="32" height="3" rx="1.5" transform="rotate(-45 18 39.8996)" fill="#FDBA30"/>
						<rect x="40.6274" y="42.728" width="32" height="3" rx="1.5" transform="rotate(-135 40.6274 42.728)" fill="#FDBA30"/>
						</svg>
					</button>
					<div>
						<div className={css.facultad}>Facultad de {titulo_facultad}</div>
						<div className={css.titulo}>CARRERAS</div>
						<hr/>
					</div>
					{ facultad=="arquitecturadisegno" &&
					<ul>
						<li>Arquitectura y Diseño de Interiores</li>
						<li>Arquitectura y Urbanismo</li>
						<li>Diseño Industrial</li>
					</ul>
					}
					{ facultad=="ingenieria" &&
					<ul>
						<li>Ingeniería Industrial</li>
						<li>Ingeniería de Sistemas Comp</li>
						<li>Ingeniería Empresarial</li>
						<li>Ingeniería Agroindustrial</li>
						<li>Ingeniería Civil</li>
						<li>Ingeniería Ambiental</li>
						<li>Ingeniería de Minas</li>
						<li>Ingeniería Geológica</li>
						<li>Ingeniería Electrónica*</li>
						<li>Ingeniería Mecatrónica*</li>
						<li>Ingeniería Logística y Transporte*</li>
					</ul>
					}
					{ facultad=="salud" &&
					<ul>
						<li>Nutrición y Dietética</li>
						<li>Enfermería*</li>
						<li>Psicología*</li>
						<li>Obstetricia*</li>
						<li>Terapia Física y Rehabilitación*</li>
					</ul>
					}
					{ facultad=="comunicaciones" &&
					<ul>
					<li>Comunicación y Publicidad</li>
					<li>Comunicación y Periodismo</li>
					<li>Comunicación Audiovisual en Medios Digitales</li>
					<li>Comunicación Corporativa</li>
					<li>Comunicación</li>
					<li>Educación y Gestión del Aprendizaje</li>
					<li>Comunicación y Diseño Gráfico*</li>
					<li>Comunicación y Marketing Digital*</li>
					</ul>
					}

					{ facultad=="negocios" &&
					<ul>
						<li>Administración y Marketing</li>
						<li>Administración y Servicios Turísticos</li>
						<li>Contabilidad y Finanzas</li>
						<li>Economía</li>
						<li>Administración</li>
						<li>Administración y Negocios Internacionales</li>
						<li>Gastronomía y Gestión de Restaurantes</li>
						<li>Economía y Negocios Internacionales*</li>
						<li>Administración y Gestión Comercial*</li>
						<li>Administración Bancaria y Financiera*</li>
						<li>Administración y Gestión del Talento Humano*</li>
						<li>Administración y Gestión Pública*</li>
						<li>Adm y Gestión del Emprendimiento*</li>
					</ul>
					}
					{ facultad=="derecho" &&
					<ul>
						<li>Derecho</li>
					</ul>
					}
					{ (facultad=="negocios" || facultad=="comunicaciones" || facultad=="ingenieria" || facultad=="salud") &&
					<div className={css.disclaimer}>*Carreras sujetas a aprobación de SUNEDU</div>
					}
				</div>
			</div>
		</div>
	</>

}

export default component
