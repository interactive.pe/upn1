import { combineReducers } from 'redux';
import usuario from './usuario';

const reductor = combineReducers({
	usuario
});

export default reductor;
