import { tipos } from './accion';

const inicial = {
	id: 0,
	nombre: "",
	apellidoPaterno: "",
	apellidoMaterno: "",
	telefono1: "",
	email: ""
}


const reducer = (state = inicial, action) => {
	switch (action.type) {
		case tipos.ACTUALIZAR:
			console.debug("actualiza usuario")
			return action.payload

		default: return state
	}
};

export default reducer;
