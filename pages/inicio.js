
import { useState, useEffect, useRef, useLayoutEffect } from 'react';
import { useRouter } from 'next/router'

import classnames from 'classnames'
import Pagina from 'components/layouts/pagina'
import css from './sass/inicio.module.sass'

import { useDispatch } from 'react-redux'
import { modificarFacultad } from 'store/sesion/accion'

// import * as nga from "next-ga";
// import { event } from "next-ga/dist/analytics/prod";

import { registrarEvento } from 'lib/ga'

export default function Inicio() {

	const router = useRouter()

	const puntoClick = (facultad) => {
		// alert("facultad")
		const href = `/facultades/${facultad}`

		registrarEvento("inicio", "facultad")
		router.push(href, href, { shallow: true });
	}

	const dispatch = useDispatch()
	dispatch(modificarFacultad(""))

	const [proporcion, cambiarProporcion] = useState(1)
	const proporcionImagen = 1440 / 900
	useEffect(() => {
		const windowResize = evt => {
			cambiarProporcion(window.window.innerWidth/window.innerHeight)
		}
		cambiarProporcion(window.window.innerWidth/window.innerHeight)

		window.addEventListener("resize", windowResize)

		return () => {
			window.removeEventListener("resize", windowResize)
		}
	},[]);


	const [marcador, cambiarMarcador] = useState("")


	const mapa = useRef();
	const pan = useRef();

	const [scrollIzquierda, actualizarScrollIzquierda] = useState(false)
	const [scrollDerecha, actualizarScrollDerecha] = useState(false)


	const revisarScroll = () => {
		const panorama = pan.current
		if(parseInt(panorama.scrollLeft) < 1){
			actualizarScrollIzquierda("|")
		}
		else{
			actualizarScrollIzquierda("-")
		}

		// actualizarScrollIzquierda((parseInt(panorama.scrollLeft) == 0)?true:false)
		// actualizarScrollDerecha(!!(parseInt(panorama.scrollLeft) === parseInt(panorama.scrollWidth - panorama.clientWidth)))

		// console.debug(scrollIzquierda, scrollDerecha)
		// console.debug(parseInt(panorama.scrollLeft) == 0)
		// console.debug(parseInt(panorama.scrollLeft), parseInt(panorama.scrollWidth - panorama.clientWidth))
	}

	const actualizarScroll = () => {
		// console.debug(scrollIzquierda, scrollDerecha)
		const panorama = pan.current
		if (scrollIzquierda) {
			panorama.scrollLeft -= 150
		}

		if (scrollDerecha) {
			panorama.scrollLeft += 150
		}
	}

	useLayoutEffect(() => {

		const interval = setInterval(
			() => actualizarScroll(),
			50
		)

		return () => {
			clearInterval(interval);
		}

	});

	useLayoutEffect(() => {

		console.debug(scrollIzquierda, scrollDerecha)


		// revisarScroll()
		// const panorama = pan.current

		// const panoramaScroll = evt => {
		// 	revisarScroll()
		// }
		// panorama.addEventListener("scroll", panoramaScroll);

		// // revisarScroll()

		// return () => {
		// 	panorama.removeEventListener("scroll", panoramaScroll);
		// }
	},[]);

	return <>
		<Pagina>
			<div className={css.panorama} ref={pan}>
				<button
					type="button"
					className={classnames(css.flecha,css.izquierda,{[css.activa]:scrollIzquierda})}
					onPointerDown={() => actualizarScrollIzquierda(true)}
					onPointerUp={() => actualizarScrollIzquierda(false)}
					onPointerLeave={() => actualizarScrollIzquierda(false)}
					>
					<svg width="60" height="61" viewBox="0 0 60 61" fill="none" xmlns="http://www.w3.org/2000/svg">
						<circle cx="30" cy="30" r="30" fill="black" fillOpacity="0.5"/>

						<rect x="16" y="29.6274" width="32" height="3" rx="1.5" transform="rotate(-45 16 29.6274)" fill="white"/>
						<rect x="38.7531" y="52.3744" width="32" height="3" rx="1.5" transform="rotate(-135 38.7531 52.3744)" fill="white"/>

					</svg>


				</button>
				<button
					type="button"
					className={classnames(css.flecha,css.derecha,{[css.activa]:scrollDerecha})}
					onPointerDown={() => actualizarScrollDerecha(true)}
					onPointerUp={() => actualizarScrollDerecha(false)}
					onPointerLeave={() => actualizarScrollDerecha(false)}
					>
					<svg width="60" height="61" viewBox="0 0 60 61" fill="none" xmlns="http://www.w3.org/2000/svg">
						<circle cx="30" cy="30" r="30" transform="rotate(-180 30 30)" fill="black" fillOpacity="0.3"/>

						<rect x="44" y="30.3726" width="32" height="3" rx="1.5" transform="rotate(135 44 30.3726)" fill="white"/>
						<rect x="21.2471" y="7.62563" width="32" height="3" rx="1.5" transform="rotate(45 21.2471 7.62563)" fill="white"/>
					</svg>

				</button>
				<svg
					viewBox="0 0 1440 900"
					className={classnames(
						css.facultades,
						{
							[css.vertical]:proporcion < proporcionImagen
						}
					)}
					ref={mapa}
					xmlns="http://www.w3.org/2000/svg"
					xmlnsXlink="http://www.w3.org/1999/xlink">
					<defs>
						<g id="punto">
							<g className={css.punto}>
								<line x1="0" y1="0" x2="0" y2="-120" strokeWidth="1"/>
								<circle cx="0" cy="0" r="45.5517" fill="transparent" stroke="#FDBA30" strokeOpacity="0.1" strokeWidth="2" fill="rgba(0,0,0,0)">
								</circle>
								<circle cx="0" cy="0" r="45.5517" fill="transparent" stroke="#FDBA30" strokeOpacity="0.1" strokeWidth="1" fill="rgba(0,0,0,0)">
									<animateTransform attributeName="transform" type="scale" additive="sum" values="1; 1.7; 1; 0; 0" begin="0s" dur="3s" repeatCount="indefinite"></animateTransform>
								</circle>
								<circle cx="0" cy="0" r="45.5517" fill="transparent" stroke="#FDBA30" strokeOpacity="0.2" strokeWidth="1" fill="rgba(0,0,0,0)">
									<animateTransform attributeName="transform" type="scale" additive="sum" values="1; 1.7; 1; 0; 0" begin="0s" delay="2s" dur="3s" repeatCount="indefinite"></animateTransform>
								</circle>
								<circle cx="0" cy="0" r="29" fill="transparent" stroke="#fdba30" strokeOpacity="0.2" strokeWidth="2"/>
								<circle cx="0" cy="0" r="22.265" fill="none" stroke="#fdba30" strokeOpacity="0.5" strokeWidth="2"/>
								<circle cx="0" cy="0" r="14.918" fill="none" stroke="#fdba30" strokeOpacity="0.7" strokeWidth="2"/>
								<circle cx="0" cy="0" r="7.347" fill="rgb(253,186,48)"/>
							</g>
						</g>
					</defs>
					<image href="/img/panorama.jpg" x="0" y="0" height="900" width="1440"/>
					<g
						className={classnames(css.marcador, {[css.hover]: marcador=="derecho" })}
						onClick={() => puntoClick("derecho")}
						onMouseEnter={()=>cambiarMarcador("derecho")}
						onMouseLeave={()=>cambiarMarcador("")}

						>
						<use x="87" y="418" xlinkHref="#punto" className={css.circulos}/>
						<rect x={87-60} y={418-150}
									fill="rgb(253,186,48)"
									width="120" height="30"
									rx="20" ry="20"/>
						<text x="87" y={418-135}
							text-align="center"
							textAnchor="middle"
							alignmentBaseline="middle"
							fontFamily="Poppins"
							fontWeight="700"
							fontSize="14">
							DERECHO
						</text>
					</g>

					<g
						className={classnames(css.marcador, {[css.hover]: marcador=="comunicaciones" })}
						onClick={() => puntoClick("comunicaciones")}
						onMouseEnter={()=>cambiarMarcador("comunicaciones")}
						onMouseLeave={()=>cambiarMarcador("")}

						>
						<use x="504" y="569" xlinkHref="#punto"  className={css.circulos}/>
						<rect x={504-80} y={569-150}
									fill="rgb(253,186,48)"
									width="160" height="30"
									rx="20" ry="20"/>
						<text x="504" y={569-135}
							text-align="center"
							textAnchor="middle"
							alignmentBaseline="middle"
							fontFamily="Poppins"
							fontWeight="700"
							fontSize="14">
							COMUNICACIONES
						</text>
					</g>

					<g
						className={classnames(css.marcador, {[css.hover]: marcador=="ingenieria" })}
						onClick={() => puntoClick("ingenieria")}
						onMouseEnter={()=>cambiarMarcador("ingenieria")}
						onMouseLeave={()=>cambiarMarcador("")}

						>
						<use x="741" y="358" xlinkHref="#punto"  className={css.circulos}/>
						<rect x={741-60} y={358-150}
									fill="rgb(253,186,48)"
									width="120" height="30"
									rx="20" ry="20"/>
						<text x="741" y={358-135}
							text-align="center"
							textAnchor="middle"
							alignmentBaseline="middle"
							fontFamily="Poppins"
							fontWeight="700"
							fontSize="14">
							INGENIERÍA
						</text>
					</g>

					<g
						className={classnames(css.marcador, {[css.hover]: marcador=="arquitecturadisegno" })}
						onClick={() => puntoClick("arquitecturadisegno")}
						onMouseEnter={()=>cambiarMarcador("arquitecturadisegno")}
						onMouseLeave={()=>cambiarMarcador("")}

						>
						<use x="892" y="440" xlinkHref="#punto"  className={css.circulos}/>
						<rect x={892-65} y={440-160}
									fill="rgb(253,186,48)"
									width="130" height="40"
									rx="20" ry="20"/>
						<text x="892" y={440-140}
							text-align="center"
							textAnchor="middle"
							alignmentBaseline="middle"
							fontFamily="Poppins"
							fontWeight="700"
							fontSize="14">
							<tspan x="892" dy="0em">ARQUITECTURA</tspan>
							<tspan x="892" dy="1em">Y DISEÑO</tspan>
						</text>
					</g>


					<g
						className={classnames(css.marcador, {[css.hover]: marcador=="negocios" })}
						onClick={() => puntoClick("negocios")}
						onMouseEnter={()=>cambiarMarcador("negocios")}
						onMouseLeave={()=>cambiarMarcador("")}

						>
						<use x="1070" y="353" xlinkHref="#punto"  className={css.circulos}/>
						<rect x={1070-80} y={353-150}
									fill="rgb(253,186,48)"
									width="160" height="30"
									rx="20" ry="20"/>
						<text x="1070" y={353-135}
							text-align="center"
							textAnchor="middle"
							alignmentBaseline="middle"
							fontFamily="Poppins"
							fontWeight="700"
							fontSize="14">
							NEGOCIOS
						</text>
					</g>

					<g
						className={classnames(css.marcador, {[css.hover]: marcador=="salud" })}
						onClick={() => puntoClick("salud")}
						onMouseEnter={()=>cambiarMarcador("salud")}
						onMouseLeave={()=>cambiarMarcador("")}

						>
						<use x="1284" y="423" xlinkHref="#punto"  className={css.circulos}/>
						<rect x={1284-80} y={423-150}
									fill="rgb(253,186,48)"
									width="160" height="30"
									rx="20" ry="20"/>
						<text x="1284" y={423-135}
							text-align="center"
							textAnchor="middle"
							alignmentBaseline="middle"
							fontFamily="Poppins"
							fontWeight="700"
							fontSize="14">
							SALUD
						</text>
					</g>
				</svg>
			</div>
		</Pagina>
	</>
}
