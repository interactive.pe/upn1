
import withRedux from 'next-redux-wrapper';
import { Provider } from 'react-redux';
import { wrapper as almacen } from '../store/';

import { useEffect } from 'react';
import { useRouter } from "next/router";
import { iniciarGA, registrarPaginaActual, registrarPagina } from '../lib/ga'

import 'bootstrap/dist/css/bootstrap.min.css';
import '../styles/global.css'

function App({ Component, pageProps }) {
	const router = useRouter()

	useEffect(() => {
		iniciarGA()
		registrarPaginaActual()

		const routeChangeComplete = (url) => {
			// console.log("routeChangeComplete", url)
			registrarPagina(url)
		}

		router.events.on("routeChangeComplete", routeChangeComplete)
		return () => {
			router.events.off("routeChangeComplete", routeChangeComplete)
		}


	},[])

	return (
		<Component {...pageProps} />
	)
}

// export default withGA("G-CE5RRYHKF7", Router)(almacen.withRedux(App));
export default almacen.withRedux(App);
