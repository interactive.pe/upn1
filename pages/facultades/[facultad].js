import { useRouter } from 'next/router'
import { useState, useRef, useEffect  } from 'react';
import Pagina from 'components/layouts/pagina'
import Puntos from 'components/puntos'
import Ganaste from 'components/ganaste'
import Videos from 'components/videos'
import Carreras from 'components/carreras'
import Juego from 'components/juego'

import { obtenerFacultad } from 'lib/api'

import css from './facultad.module.sass'
import classnames from 'classnames'

import { useDispatch } from 'react-redux'
import { modificarFacultad } from 'store/sesion/accion'
import { actualizarPuntaje, consultarPuntajeJuego } from 'store/puntaje/accion'

import { registrarEvento } from 'lib/ga'
// const proporionImagen = 1440 / 900

const usePrevious = (value) => {
	const ref = useRef();
	useEffect(() => {
		ref.current = value;
	});
	return ref.current;
}

export default function Facultad() {
	// console.log(params)
	const router = useRouter()
	const { facultad } = router.query
	const contenido = obtenerFacultad(facultad)
	// console.log(facultad)
	// console.log(obtenerFacultad)
	// console.debug(contenido)

	const dispatch = useDispatch()
	dispatch(modificarFacultad(facultad))

	const [cuadro, cambiarCuadro] = useState("ninguno")
	const [movil, cambiarMovil] = useState(false)
	const [proporcion, cambiarProporcion] = useState(1)
	const [proporcionImagen, cambiarProporcionImagen] = useState(1)
	const [marcador, cambiarMarcador] = useState("")

	const pan = useRef()

	useEffect(() => {
		// console.debug(pan);
		// pan.current
	},[]);

	useEffect(() => {
		dispatch(modificarFacultad(facultad))

		const matchMediaChange = e => { cambiarMovil(e.matches) }
		cambiarMovil(window.matchMedia("(max-width: 768px)").matches)
		window.matchMedia("(max-width: 768px)").addListener( matchMediaChange)

		const windowResize = evt => {
			cambiarProporcion(window.window.innerWidth/window.innerHeight)
		}
		cambiarProporcion(window.window.innerWidth/window.innerHeight)

		window.addEventListener("resize", windowResize)

		return () => {
			window.matchMedia("(max-width: 768px)").removeListener(matchMediaChange)
			window.removeEventListener("resize", windowResize)
		}
	},[]);

	const mapa = useRef()
	useEffect(() => {
		cambiarProporcionImagen(movil?414/736:1440/900)

	}, [movil])

	const prevCuadro = usePrevious(cuadro)
	useEffect(() => {

		// event("navegacion", "click " + cuadro)
		if (cuadro == "ninguno") {
			registrarEvento("navegacion " + facultad, "cerrar " + prevCuadro)
			// console.info("navegacion cerrar",  prevCuadro + " > " + cuadro)
		}
		else{
			registrarEvento("navegacion " + facultad, "abrir " + cuadro)
			// console.info("navegacion abrir",  prevCuadro + " > " + cuadro)
		}

	}, [cuadro])


	const footer=()=>(
		<div className={css.footer}>
			<h3>Bienvenido a la Facultad de</h3>
			<h1>{contenido.titulo}</h1>
		</div>
	)

	const marcadorClick=(item)=>{
		cambiarCuadro(item)
	}
	const marcadores = contenido && contenido.puntos.map(item => (
		<g className={classnames(css.marcador, css.escritorio, {[css.hover]: marcador==item.url})}
			key={item.url}
			transform={`translate(${item.x}, ${item.y})`}
			onMouseEnter={()=>cambiarMarcador(item.url)}
			onMouseLeave={()=>cambiarMarcador("")}
			onClick={()=>marcadorClick(item.url)}>
			<use x="0" y="0" xlinkHref="#marcador" className={css.circulos}/>
			<rect x="-90" y="55"
					width="180" height="40"
					rx="20" ry="20"/>
			<text x="0" y="75"
				textAnchor="middle"
				alignmentBaseline="middle"
				fontFamily="Poppins"
				fontWeight="bold"
				fontSize="14">
				{item.texto}
			</text>
		</g>
	))
	const marcadoresMovil = contenido && contenido.puntosMovil.map(item => (
		<g className={classnames(css.marcador, css.movil)}
			key={item.url}
			transform={`translate(${item.x}, ${item.y})`}
			onClick={()=>marcadorClick(item.url)}>
			<use x="0" y="0" xlinkHref="#marcadorMovil" className={css.circulos}/>
			<rect x="-60" y="35"
					fill="black"
					width="120" height="27"
					rx="15" ry="15"/>
			<text x="0" y="50"
				fill="#FDBA30"
				textAnchor="middle"
				alignmentBaseline="middle"
				fontFamily="Poppins"
				fontWeight="700"
				fontSize="12">
				{item.texto}
			</text>
		</g>
	))

	const cerrarCuadro = evt => {
		cambiarCuadro("ninguno")
	}


	const cerrarJuego = (juego) => {
		console.debug("cerrarJuego", juego)
		if(juego){
			dispatch(consultarPuntajeJuego())
		}
		cambiarCuadro("ninguno")
	}

	const cuadros= <>
		<Choose>
			<When condition={(cuadro == "puntos")}>
				<Puntos cerrar={()=>cambiarCuadro("ninguno")} />
			</When>

			<When condition={(cuadro == "testimoniales")}>
				<Videos videos={contenido.testimoniales} facultad={contenido.titulo} titulo="Testimoniales" cerrar={cerrarCuadro} />
			</When>

			<When condition={(cuadro == "charlas")}>
				<Videos videos={contenido.testimoniales} charla={contenido.charla} facultad={contenido.titulo} titulo="Charlas" cerrar={cerrarCuadro} />
			</When>

			<When condition={(cuadro == "carreras")}>
				<Carreras cerrar={()=>cambiarCuadro("ninguno")} facultad={contenido.nombre} titulo_facultad={contenido.titulo} />
			</When>
			<When condition={(cuadro == "videojuego")}>
				<Juego cerrar={cerrarJuego} nombreFacultad={contenido.nombre}/>
			</When>
		</Choose>
	</>

	return <>
		<If condition={contenido}>
		<Pagina className={css.pagina} footer={footer()} cuadros={cuadros}>
			<div className={css.panorama} ref={pan}>
				<svg
					viewBox={movil?"0 0 414 736":"0 0 1440 900"}
					ref={mapa}
					className={classnames(
						css.mapa,
						{
							[css.vertical]:proporcion < proporcionImagen
						}
					)}
					xmlns="http://www.w3.org/2000/svg"
					xmlnsXlink="http://www.w3.org/1999/xlink">
					<defs>
						<g id="marcador" className={css.punto}>

							<circle cx="0" cy="0" r="45.5517" fill="transparent" stroke="#FDBA30" strokeOpacity="0.2" strokeWidth="2">
							</circle>
							<circle cx="0" cy="0" r="45.5517" fill="transparent" stroke="#FDBA30" strokeOpacity="0.2" strokeWidth="1">
								<animateTransform attributeName="transform" type="scale" additive="sum" values="1; 1.7; 1; 0; 0" begin="0s" dur="4s" repeatCount="indefinite"></animateTransform>
							</circle>
							<circle cx="0" cy="0" r="45.5517" fill="transparent" stroke="#FDBA30" strokeOpacity="0.2" strokeWidth="1">
								<animateTransform attributeName="transform" type="scale" additive="sum" values="1; 1.7; 1; 0; 0" begin="0s" delay="0.1s" dur="4s" repeatCount="indefinite"></animateTransform>
							</circle>

							<circle cx="0" cy="0" r="35.1013" fill="transparent" stroke="#FDBA30" strokeOpacity="0.5" strokeWidth="2"/>
							<circle cx="0" cy="0" r="23.7009" fill="transparent" stroke="#FDBA30" strokeOpacity="0.7" strokeWidth="2"/>
							<circle cx="0" cy="0" r="11.4004" className="centro"/>
						</g>
						<g id="marcadorMovil" className={css.punto}>
							<circle cx="0" cy="0" r="29" fill="transparent" stroke="#FDBA30" strokeOpacity="0.2" strokeWidth="1">
								<animateTransform attributeName="transform" type="scale" additive="sum" values="1; 1.7; 1; 0; 0" begin="0s" dur="3s" repeatCount="indefinite"></animateTransform>
							</circle>

							<circle cx="0" cy="0" r="29" fill="transparent" stroke="#FDBA30" strokeOpacity="0.2" strokeWidth="1">
								<animateTransform attributeName="transform" type="scale" additive="sum" values="1; 1.7; 1; 0; 0" begin="0s" delay="0.1s" dur="3s" repeatCount="indefinite"></animateTransform>
							</circle>
								<circle cx="0" cy="0" r="29" fill="transparent" stroke="#FDBA30" strokeOpacity="0.2" strokeWidth="2"/>
								<circle cx="0" cy="0" r="22.2653" fill="transparent" stroke="#FDBA30" strokeOpacity="0.5" strokeWidth="2"/>
								<circle cx="0" cy="0" r="14.9184" fill="transparent" stroke="#FDBA30" strokeOpacity="0.7" strokeWidth="2"/>
								<circle cx="0" cy="0" r="7.34694" fill="#FDBA30"/>
						</g>
						<g id="puntos">
							<circle cx="0" cy="0" r="29" fill="none" stroke="#FF0000" strokeWidth="2"/>
							<circle cx="0" cy="0" r="22.2653" fill="none" stroke="#FF0000" strokeWidth="2"/>
							<circle cx="0" cy="0" r="14.9184" fill="none" stroke="#FF0000" strokeWidth="2"/>
							<circle cx="0" cy="0" r="7.34694" fill="none" fill="#FF0000"/>
							<rect x="-178" y="-13.5" width="140" height="27" rx="13.5" fill="black"/>
							<text x="-165" y="0"
								fill="#FF0000"
								textAnchor="start"
								alignmentBaseline="middle"
								fontFamily="Poppins"
								fontWeight="bold"
								fontSize="12">
								Acumula puntos
							</text>
						</g>

					</defs>

					<image className={css.escritorio} href={`/img/${contenido.imagen}`} x="0" y="0" height="900" width="1440"/>
					<image className={css.movil} href={`/img/${contenido.imagenMovil}`} x="0" y="0" height="736" width="414"/>
					{marcadores}
					{marcadoresMovil}
					<use x="1381" y="431" xlinkHref="#puntos"
						className={css.marcadorPuntos}
						onClick={()=>marcadorClick("puntos")}/>
				</svg>
			</div>
			<button type="button" className={css.btnPuntos} onClick={()=>marcadorClick("puntos")} >
				<span>Acumula puntos</span>
				<svg width="60" height="60" viewBox="0 0 60 60" fill="none" xmlns="http://www.w3.org/2000/svg">
					<circle cx="30" cy="30" r="29" stroke="#FF0000" strokeWidth="2"/>
					<circle cx="30" cy="30.0002" r="22.2653" stroke="#FF0000" strokeWidth="2"/>
					<circle cx="29.9999" cy="29.9999" r="14.9184" stroke="#FF0000" strokeWidth="2"/>
					<circle cx="30" cy="30" r="7.34694" fill="#FF0000"/>
				</svg>

			</button>
		</Pagina>
		</If>
	</>
}
