import Simple from '@/components/layouts/simple'
import css from './sass/login.module.sass'
import classnames from 'classnames'
import Login from '@/components/login'


import Container from 'react-bootstrap/Container'
import Carousel from 'react-bootstrap/Carousel'
import Image from 'react-bootstrap/Image'


var fase = 1

const fondo = ()=>(
	<Carousel className={css.fondoCarrusel} controls={false} fade={true} indicators={false}>
		<Carousel.Item>
			<figure>
				<picture>
					<source media="(max-width: 768px)" srcSet="/img/fondo1.jpg" />
					<source media="(min-width: 768px)" srcSet="/img/inicio_bg_1.jpg" />
					<Image className={css.logo} src="/img/fondo1.jpg" fluid/>
				</picture>
				<figcaption>
					<p>Podrás ganar vales de <em>Plaza Vea</em>, suscripciones en <strong>Spotify</strong> y tarjetas de alimentos <em>Endenred</em></p>
					<small>* Válido en locales afiliados en todo el pais como KFC, Pizza Hut y muchos mas</small>
				</figcaption>
			</figure>
		</Carousel.Item>
		<Carousel.Item>
			<figure>
				<picture>
					<source media="(max-width: 768px)" srcSet="/img/fondo2.jpg" />
					<source media="(min-width: 768px)" srcSet="/img/inicio_bg_2.jpg" />
					<Image className={css.logo} src="/img/fondo2.jpg" fluid/>
				</picture>
				<figcaption>
					<p>Visita nuestras</p>
					<p>Facultades</p>
					<p>Asiste a las charlas y participa en divertidos juegos</p>
				</figcaption>
			</figure>
		</Carousel.Item>
	</Carousel>
)


const header = ()=>(
	<Image className={css.logo} src="/img/logotema.png" fluid/>
)


const Pagina = () => (
	<Simple className={css.pagina} class="bgAmarillo" fondo={fondo()} header={header()}>
		<Container fluid>
			<Login/>
		</Container>
	</Simple>
)
export default Pagina;
